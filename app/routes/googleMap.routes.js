module.exports = app => {
    const googleMapController = require("../controller/googleMap.controller")
    app.get("/google/startTextSearchQuery", googleMapController.startTextSearchQuery);
    app.get("/google/startPlaceDetailsQuery", googleMapController.startPlaceDetailsQuery);
    app.get("/google/extractCityNameFromPlaceDetails", googleMapController.extractCityNameFromPlaceDetails);
};
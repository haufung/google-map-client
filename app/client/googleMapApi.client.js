const request = require('request');
const qs = require("querystring");

exports.queryGoogleTextSearch = (paramObj) => {
    const params = qs.stringify(paramObj);
    return new Promise((resolve, reject) => {
        let requestUrl = `${process.env.GOOGLE_API_TEXT_SEARCH}${params}`
        const options = {
            url: requestUrl,
            method: 'GET'
        }
        console.log("Outbound Request: " + JSON.stringify(options))
        request(options, (err, response) => {
            if (err) {
                reject(err);
                console.log("Response Error: " + JSON.stringify(err))
            } else {
                resolve(JSON.parse(response.body))
            }
        })
    })
}

exports.queryGooglePlaceDetailsSearch = (paramObj) => {
    const params = qs.stringify(paramObj);
    return new Promise((resolve, reject) => {
        let requestUrl = `${process.env.GOOGLE_API_PLACE_DETAILS}${params}`
        const options = {
            url: requestUrl,
            method: 'GET'
        }
        console.log("Outbound Request: " + JSON.stringify(options))
        request(options, (err, response) => {
            if (err) {
                reject(err);
                console.log("Response Error: " + JSON.stringify(err))
            } else {
                resolve(JSON.parse(response.body))
            }
        })
    })
}
const db = require('../model/db')
const { v4: uuidv4 } = require('uuid');

exports.insertNewJob = () => {
    let job_id = uuidv4()
    return new Promise((resolve, reject) => {
        var sqlQuery = {
            text: 'INSERT INTO job_progress (job_id, status) VALUES ($1,$2);',
            values: [job_id, 'PROGRESSING'],
          }
        db.query(sqlQuery, (err, dbRes) => {
          if (err) {
            console.error('jobProgressUtil.insertNewJob :', err);
            reject()
          } else {
            resolve(job_id)
          }
        })
    })
}

exports.updateJobStatus = (job_id, status) => {
    return new Promise((resolve, reject) => {
        var sqlQuery = {
            text: "UPDATE job_progress SET status = $1 where job_id = $2;",
            values: [status, job_id],
          }
        db.query(sqlQuery, (err, dbRes) => {
          if (err) {
            console.error('jobProgressUtil.updateJobToFailed :', err);
            reject()
          } else {
            resolve()
          }
        })
    })
}

exports.queryProgressingJobId = () => {
    return new Promise((resolve, reject) => {
        var sqlQuery = {
            text: "SELECT job_id from job_progress where status = 'PROGRESSING';"
          }
        db.query(sqlQuery, (err, dbRes) => {
          if (err) {
            console.error('jobProgressUtil.insertNewJob :', err);
            reject()
          } else if (dbRes.rows.length > 1) {
            console.error('jobProgressUtil.insertNewJob : More than one job is PROGRESSING');
            reject("More than one job is PROGRESSING")
          } else {
            resolve(dbRes.rows[0].job_id)
          }
        })
    })
}
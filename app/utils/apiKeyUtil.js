const db = require('../model/db')

module.exports.queryApiKey = () => {
    return new Promise((resolve, reject) => {
        var sqlQuery = "SELECT api_key FROM api_key;"
        db.query(sqlQuery, (err, dbRes) => {
          if (err) {
            console.error('apiKeyUtil.queryApiKey :', err);
            return reject()
          } else {
            return resolve(dbRes.rows[0].api_key)
          }
        })
    })
}
const db = require('../model/db')
const { commonResponse } = require('../common/response');
const apiKeyUtil = require('../utils/apiKeyUtil');
const jobProgressUtil = require('../utils/jobProgressUtil');
const longLatOfCenterUtil = require('../utils/longLatOfCenterUtil');
const outputResultUtil = require('../utils/outputResultUtil');
const placeDetailsUtil = require('../utils/placeDetailsUtil');
const googleMapApiClient = require('../client/googleMapApi.client');
const { time } = require('cron');
var ScrapingCompetitor

function checkIfLongLatAlreadyExist(co_ordinates_lng, co_ordinates_lat) {
  return new Promise((resolve, reject) => {
    db.query({
      text: 'SELECT count(*) FROM output_result where co_ordinates_lng = $1 and co_ordinates_lat = $2;',
      values: [co_ordinates_lng, co_ordinates_lat],
    }, (err, dbRes) => {
      if (err) {
        console.error('googleMap.checkIfLongLatAlreadyExist :', err);
        reject(err)
      } else {
        resolve(dbRes.rows[0].count > 0)
      }
    })
  })
}

async function insertIntoOutputResult(json_body, place_id, competitor_name, co_ordinates_lng, co_ordinates_lat, plus_code, address, job_id, long_lat_string, radius) {
  if (await checkIfLongLatAlreadyExist(co_ordinates_lng, co_ordinates_lat)) {
    return
  }
  return new Promise((resolve, reject) => {
    db.query({
      text: 'INSERT INTO output_result (json_body, place_id, competitor_name, co_ordinates_lng, co_ordinates_lat, plus_code, address, input_query, job_id, long_lat_string, radius) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11);',
      values: [json_body, place_id, competitor_name, co_ordinates_lng, co_ordinates_lat, plus_code, address, ScrapingCompetitor, job_id, long_lat_string, radius],
    }, (err, dbRes) => {
      if (err) {
        console.error('googleMap.insertIntoOutputResult :', err);
        reject(err)
      } else {
        resolve()
      }
    })
  })
}
async function parseTextSearchAndInsertResult(jobId, result, longLatString, radiusInMeter) {
  let nextPageToken = result.next_page_token
  if (result.results) {
    for (let obj of result.results) {
      let jsonBody = obj
      let placeId = obj.place_id
      let competitorName = obj.name
      let coOrdinatesLng = obj.geometry.location.lng
      let coOrdinatesLat = obj.geometry.location.lat
      let plusCode
      if (obj.plus_code && obj.plus_code.compound_code) {
        plusCode = obj.plus_code.compound_code
      }
      let address = obj.formatted_address
      await insertIntoOutputResult(JSON.stringify(jsonBody), placeId, competitorName, coOrdinatesLng, coOrdinatesLat, plusCode, address, jobId, longLatString, radiusInMeter)
    }
  }
  return nextPageToken
}

function queryCoffeeShopKeywords() {
  return new Promise((resolve, reject) => {
    db.query('select name from coffee_shop where scraped is not true;', (err, dbRes) => {
      if (err) {
        console.error('googleMap.queryCoffeeShopKeywords :', err);
        reject(err)
      } else {
        resolve(dbRes.rows)
      }
    })
  })
}

function calculateBiggerRadiusInMeter(circleRadius) {
  var circleRadiusInMeter = circleRadius * 111 * 1000
  var timesTwoSquare = Math.pow(circleRadiusInMeter * 2, 2)
  var timesTwoSquareRoot = Math.sqrt(timesTwoSquare * 2)
  var radiusInMeter = timesTwoSquareRoot / 2
  return radiusInMeter
}

async function generateListOfLongLatsWithInitialSplit(jobId, topLeft, topRight, initalSplit) {

  initalSplit = parseInt(initalSplit)
  let numberOfInterval = initalSplit * 2
  let latDifference = parseFloat(topRight[1]) - parseFloat(topLeft[1])
  let circleRadius = latDifference / numberOfInterval
  let circleDiameter = circleRadius * 2
  var currentLat = parseFloat(topRight[1])
  var currentLong = parseFloat(topRight[0])
  for (let i = 0; i < initalSplit; i++) {
    currentLat = i == 0 ? currentLat - circleRadius : currentLat - circleDiameter
    currentLong = parseFloat(topRight[0])
    for (let j = 0; j < initalSplit; j++) {
      currentLong = j == 0 ? currentLong - circleRadius : currentLong - circleDiameter
      await longLatOfCenterUtil.insertLongLatForCenter(jobId, currentLong, currentLat)
    }
  }
  var biggerRadius = calculateBiggerRadiusInMeter(circleRadius)
  longLatOfCenterUtil.updateRadiusForCenterByJobId(jobId, biggerRadius)
  return biggerRadius
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

exports.startTextSearchQuery = async (req, res) => {
  let resp = new commonResponse();
  console.log('Entering googleMap.startTextSearchQuery');
  let jobId = await jobProgressUtil.insertNewJob()
  try {
    var param = req.query
    let topLeft = param.top_left.split(",")
    let topRight = param.top_right.split(",")
    let initalSplit = param.initial_split
    let radiusInMeter = await generateListOfLongLatsWithInitialSplit(jobId, topLeft, topRight, initalSplit)
    let listOfLongLats = await longLatOfCenterUtil.queryLongLatForCenterByJobId(jobId)
    let coffeeShopList = await queryCoffeeShopKeywords()
    let apiKey = await apiKeyUtil.queryApiKey()
    for (let longLats of listOfLongLats) {
      let longLatString = [longLats.long, longLats.lat].join(",")
      console.log(longLatString)
      for (let name of coffeeShopList) {
        console.log(`Getting ${name.name}...`)
        ScrapingCompetitor = name.name
        let result = await googleMapApiClient.queryGoogleTextSearch({
          query: name.name,
          region: '.hk',
          key: apiKey,
          location: longLatString,
          radius: radiusInMeter.toString()
        })
        var nextPageToken = await parseTextSearchAndInsertResult(jobId, result, longLatString, radiusInMeter)
        while (nextPageToken != undefined) {
          await sleep(2000)
          let result = await googleMapApiClient.queryGoogleTextSearch({
            query: name.name,
            region: '.hk',
            key: apiKey,
            pagetoken: nextPageToken,
            location: longLatString,
            radius: radiusInMeter.toString()
          })
          if (result.status == "OK") {
            nextPageToken = await parseTextSearchAndInsertResult(jobId, result, longLatString, radiusInMeter)
            console.log(`Next Page Token ${nextPageToken}...`)
          } else {
            throw new Error(result.status)
          }
        }
      };
    }
    res.send(resp);
    res.end();
    jobProgressUtil.updateJobStatus(jobId, 'COMPLETED')
  } catch (e) {
    console.error('googleMap.startTextSearchQuery :', e);
    jobProgressUtil.updateJobStatus(jobId, 'FAILED')
    resp.message = 'INTERNAL SERVER ERROR'
    resp.code = 500
    res.status(500).send(resp);
  }

}

exports.startPlaceDetailsQuery = async (req, res) => {
  let resp = new commonResponse();
  console.log('Entering googleMap.startPlaceDetailsQuery');
  try {
    let apiKey = await apiKeyUtil.queryApiKey()
    var allPlaceId = await outputResultUtil.queryPlaceIdWhereHaveNoPlaceDetails()
    for (let placeIdObj of allPlaceId) {
      let result = await googleMapApiClient.queryGooglePlaceDetailsSearch({
        place_id: placeIdObj.place_id,
        key: apiKey
      })
      let jsonBody = JSON.stringify(result.result)
      let placeId = result.result.place_id
      let openingHoursWeekdayText
      if (result.result.opening_hours && result.result.opening_hours.weekday_text) {
        openingHoursWeekdayText = result.result.opening_hours.weekday_text.join(" | ")
      }

      await placeDetailsUtil.insertNewRecords(jsonBody, placeId, openingHoursWeekdayText)
      await sleep(2000)
    };
    res.send(resp);
    res.end();
  } catch (e) {
    console.error('googleMap.startPlaceDetailsQuery :', e);
    resp.message = 'INTERNAL SERVER ERROR'
    resp.code = 500
    res.status(500).send(resp);
  }

}

exports.extractCityNameFromPlaceDetails = async (req, res) => {
  let resp = new commonResponse();
  console.log('Entering googleMap.extractCityNameFromPlaceDetails');
  try {
    let listOfJson = await placeDetailsUtil.queryJsonBodyWhereCityNameIsNull()
    for (strObj of listOfJson) {
      let obj = JSON.parse(strObj.json_body)
      var thisPlaceId = obj.place_id
      if (obj.address_components && obj.address_components.length > 0) {
        var cityComponent = obj.address_components.pop()
        if (!isNaN(cityComponent.long_name)) {
          cityComponent = obj.address_components.pop()
        }
        if (cityComponent.long_name == 'China') {
          cityComponent = obj.address_components.pop()
        }
        await placeDetailsUtil.udpateCityNameByPlaceId(thisPlaceId, cityComponent.long_name)
      }
    }
    res.send(resp);
    res.end();
  } catch (e) {
    console.error('googleMap.startPlaceDetailsQuery :', e);
    resp.message = 'INTERNAL SERVER ERROR'
    resp.code = 500
    res.status(500).send(resp);
  }

}
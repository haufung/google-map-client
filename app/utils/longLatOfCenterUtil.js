const db = require('../model/db')

exports.insertLongLatForCenter = (jobId, long, lat) => {
  return new Promise((resolve, reject) => {
    db.query({
      text: 'INSERT INTO long_lat_of_center (job_id, long, lat) VALUES ($1,$2,$3);',
      values: [jobId, long, lat],
    }, (err, dbRes) => {
      if (err) {
        console.error('longLatOfCircleCenterUtil.insertLongLatForCenter :', err);
        reject()
      } else {
        resolve()
      }
    })
  })
}

exports.updateRadiusForCenterByJobId = (jobId, radius) => {
  return new Promise((resolve, reject) => {
    var sqlQuery = {
      text: 'UPDATE long_lat_of_center SET radius = $1 wHERE job_id = $2;',
      values: [radius, jobId],
    }
    db.query(sqlQuery, (err, dbRes) => {
      if (err) {
        console.error('longLatOfCircleCenterUtil.updateRadiusForCenterByJobId :', err);
        reject()
      } else {
        resolve()
      }
    })
  })
}

exports.queryLongLatForCenterByJobId = (jobId) => {
  return new Promise((resolve, reject) => {
    var sqlQuery = {
      text: 'SELECT * FROM long_lat_of_center WHERE job_id = $1;',
      values: [jobId],
    }
    db.query(sqlQuery, (err, dbRes) => {
      if (err) {
        console.error('longLatOfCircleCenterUtil.queryLongLatForCenterByJobId :', err);
        reject()
      } else {
        resolve(dbRes.rows)
      }
    })
  })
}
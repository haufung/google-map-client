const { Pool } = require('pg')
const pool = new Pool({
    user: 'postgres',
    host: process.env.DB_IP_ADDRESS,
    database: 'google_map_data',
    password: process.env.DB_PASSWORD,
    port: 5432,
  })
module.exports = {
  query: (text, params, callback) => {
    return pool.query(text, params, callback)
  },
}

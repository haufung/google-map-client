const db = require('../model/db')

exports.insertNewRecords = (json_body, place_id, open_close_weekday_text) => {
    return new Promise((resolve, reject) => {
        var sqlQuery = {
            text: 'INSERT INTO place_details (json_body, place_id, open_close_weekday_text) VALUES ($1,$2,$3);',
            values: [json_body, place_id, open_close_weekday_text],
        }
        db.query(sqlQuery, (err, dbRes) => {
            if (err) {
                console.error('placeDetailsUtil.insertNewRecords :', err);
                reject()
            } else {
                resolve()
            }
        })
    })
}

exports.udpateCityNameByPlaceId = (place_id, city_name) => {
    return new Promise((resolve, reject) => {
        var sqlQuery = {
            text: 'UPDATE place_details SET city_name = $1 where place_id = $2;',
            values: [city_name, place_id],
        }
        db.query(sqlQuery, (err, dbRes) => {
            if (err) {
                console.error('placeDetailsUtil.udpateCityNameByPlaceId :', err);
                reject()
            } else {
                resolve()
            }
        })
    })
}

exports.queryJsonBodyWhereCityNameIsNull = () => {
    return new Promise((resolve, reject) => {
        var sqlQuery = {
            text: 'SELECT * FROM place_details;'
        }
        db.query(sqlQuery, (err, dbRes) => {
            if (err) {
                console.error('placeDetailsUtil.queryJsonBodyWhereCityNameIsNull :', err);
                reject()
            } else {
                resolve(dbRes.rows)
            }
        })
    })
}
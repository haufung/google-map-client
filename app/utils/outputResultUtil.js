const db = require('../model/db')

exports.queryPlaceIdWhereHaveNoPlaceDetails = () => {
    return new Promise((resolve, reject) => {
        var sqlQuery = {
            text: "SELECT place_id from output_result where place_id not in (select place_id from place_details);"
          }
        db.query(sqlQuery, (err, dbRes) => {
          if (err) {
            console.error('outputResultUtil.queryPlaceIdWhereHaveNoPlaceDetails :', err);
            reject()
          } else {
            resolve(dbRes.rows)
          }
        })
    })
}
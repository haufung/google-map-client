class commonResponse {
    constructor(code = 200, message = "success") {
        this.code = code;
        this.message = message;
        this.data = null;
    }
}
exports.commonResponse = commonResponse;

const express = require('express')
let dotenv = require("dotenv");
let path = require("path");
const app = express()
const port = 3000
const envFile = process.env.ENV_NAME ? `${process.env.ENV_NAME}.env` : "dev.env"
dotenv.config({ path: path.join(__dirname, './config', envFile) });

require("./app/routes/googleMap.routes")(app);

app.listen(port, process.env.PRIVATE_IP_ADDRESS,  () => {
  console.log(`Example app listening at http://${process.env.PRIVATE_IP_ADDRESS}:${port}`)
})

